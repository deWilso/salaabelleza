<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_usuarios')->insert([
            'tipo' => 'Administrador',
        ]);

        DB::table('tipo_usuarios')->insert([
            'tipo' => 'Estandar',
        ]);

         //usuarios
         DB::table('users')->insert([
             'cui'=>'2250303571808',
            'primer_nombre' => 'Developer',
            'telefono'=>'77231810',
            'direccion'=>'no soy de aqui ni de alla',            
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12345678'),
            'tipo_id' => '1',
        ]);

    }
}
