<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleReserva extends Model
{
    protected $table = 'detalle_reservas';
    protected $fillable = [
        'id', 'reserva_id', 'servicio_id'
    ];

    public function reserva(){
        return $this->belongsTo('App\Reserva');
    }

    public function servicio(){
        return $this->belongsTo('App\Servicio');
    }
}
