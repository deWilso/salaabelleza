<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $table = 'reserva';
    protected $fillable = [
        'id', 'fecha', 'hora','comentario', 'user_id', 'estado_id'
    ];

    public function estado(){
        return $this->belongsTo('App\Estado');
    }

    public function reservas(){
        return $this->hasMany('App\DetalleReserva');
    }

}
