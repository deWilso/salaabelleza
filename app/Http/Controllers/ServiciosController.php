<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;

class ServiciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        // dd($servicios);
        return view ('servicios.index');
    }

    public function getListaServicios()
    {
        $servicios = Servicio::orderBy('id', 'DESC')->get();
        return $servicios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $servicio = $request->servicio;
            $detalle_servicio = $request->detalle_servicio;
            $precio = $request->precio;
            $data = $request->all();

            $nuevo_servicio = new Servicio;
            $nuevo_servicio->servicio = $servicio;
            $nuevo_servicio->detalle_servicio = $detalle_servicio;
            $nuevo_servicio->precio = $precio;
            $nuevo_servicio->save();       
            return response ($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('servicios.update');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request)
    {
        $servicio = Servicio::whereId($request->id)->firstOrFail();
        return response($servicio);
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {        
            $servicio = Servicio::whereId($request->id)->firstOrFail();
            if(!$servicio){
                return response()->json([
                    'message' => 'Registro inexistente'], 422);
            } 
            else {
            $servicio->servicio =  $request->servicio;
            $servicio->detalle_servicio= $request->detalle_servicio;
            $servicio->precio= $request->precio;
            $servicio->save();
            return response($servicio);
            }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $data = $request->all();
        $servicio = Servicio::whereId($request->id)->firstOrFail();
        $servicio->delete();
        return response($data);
    }
}
