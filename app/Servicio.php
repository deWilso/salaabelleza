<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = 'servicios';
    protected $fillable = [
        'id', 'servicio', 'detalle_servicio','precio'
    ];

    public function reservas(){
        return $this->hasMany('App\DetalleReserva');
    }

}
