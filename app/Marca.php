<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = 'marcas';
    protected $fillable = [
        'id', 'marca',
    ];

    public function productos(){
        return $this->hasMany('App\Producto');
    }

}
