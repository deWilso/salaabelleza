<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
    protected $fillable = [
        'id', 'codigo', 'detalle','precio', 'stock', 'marca_id'
    ];

    public function tipo(){
        return $this->belongsTo('App\Marca');
    }
}
