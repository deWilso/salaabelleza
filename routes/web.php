<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('cliente/home-cliente');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// ?Route::get('/home-cliente', 'ClienteController@index')->name('home-cliente');
Route::get('/servicios', 'ServiciosController@index')->name('servicios');
Route::get('/servicios/getListaServicios', 'ServiciosController@getListaServicios')->name('getListaServicios');
Route::get('/productos', 'ProductosController@index')->name('productos');
Route::post('/servicios/destroy', 'ServiciosController@destroy')->name('serviciosDestroy');
Route::get('/serviciosCreate', 'ServiciosController@create')->name('serviciosCreate');
Route::post('/servicios/store', 'ServiciosController@store')->name('serviciosStore');
Route::get('/servicioEdit', 'ServiciosController@edit')->name('servicioEdit');
Route::post('/servicioUpdate','ServiciosController@update')->name('sevicioUpdate');
Route::get('/servicioShow', 'ServiciosController@show')->name('servicioShow');