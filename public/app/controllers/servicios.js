app.controller('ServicesIndex', function ($window, $scope, $http, apiUrl) {


  $scope.listaServicios = function()
  {
      $scope.loadServicios();
  }

  $scope.loadServicios = function()
  {
    $http
    ({
      method: 'GET',
      url: apiUrl + '/servicios/getListaServicios',
      params: {}
    }).then(function successCallback(response) 
    {
      $scope.servicios = response.data;
    }, function errorCallback(response) {
      
    });
  }

  $scope.deleteService = function(id, index)
  {
    Swal.fire({
      title: 'Eliminar',
      text: "Realmente desea eliminar el servicio?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Aceptar'
    }).then((result) => {
      if (result.value) {
        $scope.servicios.splice(index, 1);
        $http({
          method: 'POST',
          url: apiUrl + '/servicios/destroy',
          params: {id: id}
        }).then(function successCallback(response) {
          Swal.fire({
            type: 'success',
            title: 'Correcto',
            text: 'Servicio Eliminado con exito!',
          })       
          }, function errorCallback(response) {
            if(response.status == 422){
              Swal.fire({
                type: 'error',
                title: 'Error',
                text: response.data.message,
              })
            }
          });
        
      }
    })
   
  }

  $scope.saveNewServicio = function() 
    {
        $scope.nombreServicio;
        $scope.detalleServicio;
        $scope.precioServicio;
        // console.log($scope.nombreServicio, $scope.detalleServicio, $scope.precioServicio);
        $http({
          method: 'POST',
          url: apiUrl + '/servicios/store',
          params: {servicio: $scope.nombreServicio, detalle_servicio: $scope.detalleServicio, precio: $scope.precioServicio}
        }).then(function successCallback(response) {
            console.log(response);
            Swal.fire({
              type: 'success',
              title: 'Correcto',
              text: 'Servicio guardado con exito!',
            }).then(function(){ 
              window.location.href = apiUrl + '/servicios';
            });         
          }, function errorCallback(response) {
            if(response.status == 422){
              Swal.fire({
                type: 'error',
                title: 'Error',
                text: response.data.message,
              })
            }else{
              Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Revise que los datos sean correctos y vuelva a intentarlo!',
              })
            
            }
          });    

    }
    
      $scope.servicio = '';
      $scope.detalle = '';
      $scope.precio = '';
    $scope.serviceEdit = function (servicio)
    { 
      $scope.serviceId = servicio;
      
      console.log($scope.serviceId);
      $http({
        method: 'GET',
        url:apiUrl + '/servicioEdit',
        params:{id:$scope.serviceId}
      }).then(function successCallback(response){        
        if(response.status==200){   
          $scope.id = response.data.id;
          $scope.servicio = response.data.servicio;
          $scope.detalle = response.data.detalle_servicio;
          $scope.precio = response.data.precio;
          $('#servicios').modal('show');

        }
        
      }, function errorCallback(response){
        if(response.status == 422){
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: response.data.message,
          })
        }else{
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Revise que los datos sean correctos y vuelva a intentarlo!',
          })
        
        }
      });
     
    }


    $scope.updateSevice = function (id)
    {
      $scope.idService=id;
      $scope.serviceName =  document.getElementById("servicio").value; 
      $scope.serviceDetail = document.getElementById("detalle_servicio").value;
      $scope.servicePrice=document.getElementById("precio_servicio").value;
      // var data = {id:$scope.idService, servicio:$scope.servicio, detalle_servicio: $scope.serviceDetail, precio:$scope.servicePrice}  
      $http({
        method: 'POST',
        url:apiUrl + '/servicioUpdate',
        params:{id:$scope.idService, servicio:$scope.serviceName, detalle_servicio: $scope.serviceDetail, precio:$scope.servicePrice}
      }).then(function successCallback(response){
        console.log(response.data);
        Swal.fire({
          type: 'success',
          title: 'Correcto',
          text: 'Servicio Actualizado con exito!',
        }).then(function(){ 
          $('#servicios').modal('hide');
        });         
      }, function errorCallback(response) {
        if(response.status == 422){
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: response.data.message,
          })
        }else{
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'Revise que los datos sean correctos y vuelva a intentarlo!',
          })
        
        }
      });
    }


 


});