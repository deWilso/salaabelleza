// enviroment develop
var app = angular.module('test', ['ngAnimate'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('#{');
    $interpolateProvider.endSymbol('}');
}).constant('apiUrl', '/citassalabelleza/public');

// enviroment prod
// var app = angular.module('test', ['ngAnimate', 'bsTable'], function($interpolateProvider) {
//     $interpolateProvider.startSymbol('#{');
//     $interpolateProvider.endSymbol('}');
// }).constant('apiUrl', '');