@extends('layouts.app')

@section('title')
Actualizacion de registros
@stop

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('servicios')}}">Servicios</a></li>
              <li class="breadcrumb-item active">Actualizar Servicio</li>
            </ol>
@stop

@section('content')
<div class="row" ng-controller="ServicesIndex">
        <div class="col-12">
          
                <div class="box-body">
                                 
                    @if(count($errors)>0)
                    <div class="col-sm-12">
                      <div class="alert alert-danger">
                        <ul>
                              @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                              @endforeach
                        </ul>
                      </div>
                    </div>
                    @endif
              </div>      
        </div>   
        <!-- Horizontal Form -->
        <div class="col-12">
        <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Actualizacion de datos del servicio</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form class="form-horizontal">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="lblNombreServicio" class="col-sm-4 col-form-label">Nombre del servicio: </label>
                    <div class="col-sm-8">
                      <input class="form-control" ng-model="servicio" name="nombre_servicio" id="nombre_servicio" placeholder="servicio" ng-value="servicio">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="lblDetalleServicio" class="col-sm-4 col-form-label">Detalle del servicio: </label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control"  ng-model="detalle" name="detalle_servicio" id="detalle_servicio" placeholder="...." ng-value="detalle">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="lbPrecioServicio" class="col-sm-4 col-form-label">Precio del servicio: </label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" ng-model="precio" name="precio_servicio" id="precio_servicio" placeholder="Q.00.00" ng-model="precio">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="button" ng-click="updateService()"class="btn btn-info">Guardar</button>
                  <a  href="{{route('servicios')}}" data-toggle="tooltip" data-placement="top" title="Cancelar" class="btn btn-default float-right">Cancelar</a>

                </div>
                <!-- /.card-footer -->
              </form>
            </div>

            </div>
   
@stop

@section('library')
    <!-- AngularJS Application Scripts -->
    <script src="{{asset('app/app.js')}}"></script>
    <!-- AngularJS Application Scripts -->
    <script src="{{asset('app/controllers/servicios.js')}}"></script>



@stop
