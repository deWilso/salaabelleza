@extends('layouts.app')

@section('title')
Lista de servicios
@stop

@section ('description')
Lista general de todos los servicios actuales del salon
@stop

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Servicios</li>
            </ol>
@stop

@section('content')
<div class="row" ng-controller="ServicesIndex" ng-init="listaServicios()">
        <div class="col-12">
          <div class="box-header">
            <a href="{{route('serviciosCreate')}}" data-toggle="tooltip" data-placement="top" title="Nuevo Servicio" class="btn btn-primary">Agregar Servicio</a>
          			
              
          </div> <br>
                <div class="box-body">
                                 
                    @if(count($errors)>0)
                    <div class="col-sm-12">
                      <div class="alert alert-danger">
                        <ul>
                              @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                              @endforeach
                        </ul>
                      </div>
                    </div>
                    @endif
              </div>      
        </div>   


        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista de servicios disponibles en sala</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="listaServicios" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Servicio</th>
                  <th>Detalle</th>
                  <th>Precio</th>
                  <th>Accion</th>
                  
                </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="servicio in servicios">
                   <td>#{ servicio.servicio}</td>
                   <td>#{ servicio.detalle_servicio }</td>
                   <td>#{ servicio.precio}</td>
                   <td>
                   <button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar" ng-click="deleteService(servicio.id, $index)"><span class="fas fa-trash"></span></button>
                   <button type="button" class="btn btn-primary" title="Editar"data-toggle="modal" ng-click="serviceEdit(servicio.id)"> <span class="fas fa-edit"></span></button>
                   </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div> 
          <!-- Card -->
        </div>
        <!-- col -->
          @include("servicios.serviciosUpdate");
@stop

@section('library')
    <!-- AngularJS Application Scripts -->
    <script src="{{asset('app/app.js')}}"></script>
    <!-- AngularJS Application Scripts -->
    <script src="{{asset('app/controllers/servicios.js')}}"></script>
    <script>
setTimeout(function () {
  $(function () {
    $('#listaServicios').DataTable({
      "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"}
    });
  });
},200);
</script>

@stop

  