<div class="modal" id="servicios">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
        
            <div class="modal-header">
            Editar Servicio
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             
            </div>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            
            <div class="modal-body">
            <form class="form-horizontal" name="formulario" id="formulario"method="POST" action="#">
            {{csrf_field()}}
            @if(count($errors)>0)
              <div class="col-sm-12">
                <div class="alert alert-danger">
                  <ul>
                        @foreach($errors->all() as $error)
                          <li>{{$error}}</li>
                        @endforeach
                  </ul>
                </div>
              </div>
              @endif
            <div class="box-body">
                <div class="form-group">
                  <label for="lblServicio" class="col-sm-2 control-label"> Servicio: </label>
                  <input type="hidden" name ="id" class="form-control" id="id" placeholder="Nuombre del servicio" ng-model="idService" ng-value="id">

                  <div class="col-sm-6">
                    <input type="text" name ="servicio" class="form-control" id="servicio" placeholder="Nuombre del servicio" ng-model="serviceName" ng-value="servicio">
                  </div>    
                </div>
                <div class="form-group">
                 <label for="lblDetalle" id="lblDetalle" class="col-sm-2 control-label">Descripciol del Serivicio: </label>
                    <div class="col-sm-6">
                        <input type="text" name ="detalle_servicio" class="form-control" id="detalle_servicio" ng-model="serviceDetail" placeholder="Agregue una descipcion del sericio" ng-value="detalle">
                    </div>   
                 </div>
                <div class="form-group">
                  <label for="lblPrecio" class="col-sm-2 control-label">Precio</label>

                  <div class="col-sm-3">
                    <input type="text" name ="precio_servicio" class="form-control" id="precio_servicio" ng-model="servicePrice" placeholder="Q.00.00" ng-value="precio">
                  </div>    
                 </div>
            
            <div class="modal-footer" style="margin-top: 15px; text-align: center">
              <input type="button" id="cancelar" class="btn btn-danger" data-dismiss="modal" value="Cancelar">
              <input ng-click="updateSevice(id)" id="guardar" class="btn btn-primary" value="Guardar">
            </div>
        </form>
        </div>    
    </div>

</div>